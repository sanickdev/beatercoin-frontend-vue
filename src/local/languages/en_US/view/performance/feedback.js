export default {
    title: "Data"
    , records: "Registers"
    , form: {
        id: {
            label: "Id"
            , placeholder: "ID ... "
            , th: "Id"
        }
        , description: {
            label: "Description"
            , placeholder: "Description ..."
            , th: "Description"
        }
        , assigner: {
            label: "Assigner"
            , placeholder: "Assigner ..."
            , th: "Assigner"
        }
        , assigned: {
            label: "Assigned"
            , placeholder: "Assigned ..."
            , th: "Assigned"
        }
        , status: {
            label: "Status"
            , placeholder: "Status ..."
            , th: "Status"
        }
    }
}