# paypay-frontend-vue

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

## All dependencies
```
npm install -g @vue/cli
npm install vue-router --save
npm install vuetify --save
npm install axios --save
npm install vue2-editor --save
npm install ramda --save
npm install vuelidate --save
npm install vue-head --save
npm install vue-i18n --save
npm install vue-scrollto --save
npm install vue-mc --save
npm install vue-image-crop-upload --save
npm install sass sass-loader fibers deepmerge -D
npm install vue-swatches --save
npm install vue-infinite-loading --save
npm install quill-image-drop-module --save
npm install quill-image-resize-module --save
npm install chance --save
npm install vue2-timeago --save
npm install vue-content-loading --save
npm install moment --save
npm install vuex --save
npm install vuex-persist --save

*Pendiente*
npm install vue vue-server-renderer --save
```

## Node js version
```
10.15.3
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
